
# -*- coding: utf-8 -*-

"""Description: Project and Reproject (MSG-Disk or user area) all
                LSASAF HDF5 products.

   How it's work: - get_dataset()
                  - project_dset() or reproject_dset() due to inputs
                  - save output file to geo file (e.g. geotiff)

   How to use this script: - see the instructions in readme.

   Dependencies: - os, math, argparse, logging, h5py, numpy, osgeo

   Author: Nuno Simoes 04/05/2017
"""

from pathlib import Path
from math import pow, asin, sin, cos, atan, tan, sqrt, degrees, radians
import argparse
import logging
import h5py
import numpy as np
from osgeo import gdal, osr, gdal_array


_LOG_LEVEL_STRINGS = ['CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG']
logger = logging.getLogger()


def project_dset(dset, llx, lly, urx, ury, proj_in, mem_drv_tp, f_out):
    """Description: Assign projection to dataset and save it as geotiff.

    Parameters:
    -----------
    dset :: dataset
    llx :: lower left x - geos
    lly :: lower left y - geos
    urx :: upper right x - geos
    ury :: upper right y - geos
    proj :: proj4
    mem_drv_tp :: drive GDAL (e.g. GTiff)
    f_out :: output filename geos
    """
    # get input and output spatial reference
    in_s = osr.SpatialReference()
    in_s.ImportFromProj4(proj_in)

    # get paramteres for geo transform
    xmin, ymin, xmax, ymax = [llx, lly, urx, ury]
    nrows, ncols = np.shape(dset)
    xres = (xmax - xmin) / float(ncols)
    yres = (ymax - ymin) / float(nrows)

    # create in memory input dataset (geo)
    geotransform = (xmin, xres, 0, ymax, 0, -yres)
    geo_dset = gdal.GetDriverByName(mem_drv_tp)
    oraster = geo_dset.Create(f_out, ncols, nrows, 1, gdal.GDT_Float32)
    oraster.SetGeoTransform(geotransform)
    oraster.SetProjection(in_s.ExportToWkt())
    band = oraster.GetRasterBand(1)
    band.WriteArray(dset)


def reproject_dset(dset, llx, lly, urx, ury, proj_in, proj_out, mem_drv_tp, f_out_repro):
    """Description: reproject dset, geos to wgs84.

    Parameters:
    -----------
    dset :: dataset
    llx :: lower left x - geos
    lly :: lower left y - geos
    urx :: upper right x - geos
    ury :: upper right y - geos
    proj_in :: proj_in proj4 from geos
    proj_out :: proj out in EPSG (e.g. 4326)
    mem_drv_tp :: drive GDAL (e.g. GTiff)
    f_out_repro :: output filename for repro

    Return:
    -------
    dset_n :: output array (epsg proj out)
    ulx :: upper left x - in proj out
    uly :: upper left y - in proj out
    lrx :: lower left x - in proj out
    lry :: lower right x - in proj out
    """
    # get input and output spatial reference
    in_s = osr.SpatialReference()
    in_s.ImportFromProj4(proj_in)
    out_s = osr.SpatialReference()
    out_s.ImportFromEPSG(proj_out)
    tx = osr.CoordinateTransformation(in_s, out_s)

    # get paramteres for geo transform
    xmin, ymin, xmax, ymax = [llx, lly, urx, ury]
    nrows, ncols = np.shape(dset)
    xres = (xmax - xmin) / float(ncols)
    yres = (ymax - ymin) / float(nrows)

    # create in memory input dataset (geo)
    geotransform = (xmin, xres, 0, ymax, 0, -yres)
    geo_dset = gdal.GetDriverByName('MEM')
    oraster = geo_dset.Create('', ncols, nrows, 1, gdal.GDT_Float32)
    oraster.SetGeoTransform(geotransform)
    oraster.SetProjection(in_s.ExportToWkt())
    band = oraster.GetRasterBand(1)
    band.WriteArray(dset)

    # transform geo to wgs
    (ulx, lry, ulz) = tx.TransformPoint(llx, lly, 0.0)
    (lrx, uly, lrz) = tx.TransformPoint(urx, ury, 0.0)

    # create in memory new dataset (output)
    mem_drv = gdal.GetDriverByName(mem_drv_tp)
    err=0.05
    dset_n = mem_drv.Create(f_out_repro, int((lrx - ulx)/err),
                            int((uly - lry)/err), 1, gdal.GDT_Float32)
    dset_n_ = dset_n.GetRasterBand(1)
    dset_n_data = dset_n_.ReadAsArray()
    dset_n_data[:] = -9999.0
    dset_n_.WriteArray(dset_n_data, 0, 0)
    # calculate the new geotransform: resolution: 0.05
    new_geo = (ulx, err, 0, uly, 0, -err)
    # Set the geotrasnform
    dset_n.SetGeoTransform(new_geo)
    dset_n.SetProjection(out_s.ExportToWkt())

    # Perform the projection/resampling
    res = gdal.ReprojectImage(oraster, dset_n, in_s.ExportToWkt(),
                              out_s.ExportToWkt(),
                              gdal.GRA_NearestNeighbour)
    # return dset_n.ReadAsArray(), ulx, uly, lrx, lry


# -------------------------- Function: get_dataset ---------------------
def get_dataset(filename, product_name, lats, lons):
    """Description: Get dataset array from HDF5 filename.

    NOTE: If you want crop array please lats=[lat0,lat1] and
          lon[lon0,lon1]. If you want full disk lats=[] and lons=[].

    Parameters:
    -----------
    filename :: product filename
    product_name :: product name
    lats :: [lat0, lat1]
    lons :: [lon0, lon1]

    Return:
    -------
    dset_crop :: dataset clip array (clip for study area)
    llx, lly, urx, ury :: dataset coordinates in geostationary projection
                          (for study area)
    mval :: missing value
    """
    # Open file, get dataset and mval,sfac
    f = h5py.File(filename, "r")
    dset = f[product_name]
    mval = dset.attrs["MISSING_VALUE"]
    sfac = dset.attrs["SCALING_FACTOR"]
    # apply missing value and scaling factor
    dset_v = np.ma.masked_values(dset.value, mval)/sfac

    # get coff and loff
    coff = f.attrs['COFF']
    loff = f.attrs['LOFF']

    # get col/lin to crop dataset
    # NOTA: the col/lin from that function already have the difference (-1)
    #       because this is a py script (begin in 0)
    if lats and lons:
        # lat1 and lon1
        [col1, lin1] = get_col_lin(lats[0], lons[0], coff, loff)
        lat1 = dset_v.shape[0] - lin1
        lon1 = col1
        # lat2 and lon2
        [col2, lin2] = get_col_lin(lats[1], lons[1], coff, loff)
        lat2 = dset_v.shape[0] - lin2
        lon2 = col2

        # crop dataset
        dset_crop = dset_v[(dset_v.shape[0]-lat2):
                           (dset_v.shape[0]-lat1), lon1:lon2]
    else:
        # parameters for MSG-Disk
        lat1 = 0.
        lon1 = lat1
        lat2 = dset_v.shape[0]
        lon2 = dset_v.shape[1]
        dset_crop = dset_v

    # get dataset coordinates: llx, lly, urx and ury
    cres, lres = (3000.403165817260742, 3000.403165817260742)
    nl = dset_v.shape[0]

    lin_i, col_i = (lat1, lon1)
    llx = (col_i - coff) * cres
    lly = (lin_i - nl + loff) * lres

    lin_f, col_f = (lat2,lon2)
    urx = (col_f - coff) * cres
    ury = (lin_f - nl + loff) * lres

    # close filename
    f.close()

    # put coordinates from top left to centre pixel
    cres_centre = cres/2.
    llx = llx + cres_centre
    urx = urx + cres_centre
    lly = lly - cres_centre
    ury = ury - cres_centre
    return dset_crop, llx, lly, urx, ury, mval


# ------------------------ Function: get_col_lin -----------------------
def get_col_lin(lat, lon, COFF, LOFF):
    """Description: get the col/lin from longitude and latitude
                    for each lsasaf region.
                    This function needs optimization.
    """
    CFAC = 13642337
    LFAC = 13642337
    sub_lon = 0
    iarea = 4
    aux16 = pow(2., -16)
    p1 = 42164.
    p2 = 0.00675701
    p3 = 0.993243
    rp = 6356.5838

    if sqrt(pow(lat, 2)+pow(lon, 2)) > 80:
        return None, None
    else:
        # coordinates to radians
        lat = radians(lat)
        lon = radians(lon)

        c_lat = atan(p3 * tan(lat))
        rl = rp / sqrt(1. - p2 * pow(cos(c_lat), 2))
        r1 = p1 - rl * cos(c_lat) * cos(lon - sub_lon)
        r2 = -rl * cos(c_lat) * sin(lon - sub_lon)
        r3 = rl * sin(c_lat)
        rn = sqrt(r1**2 + r2**2 + r3**2)

        x = atan(-r2/r1)
        y = asin(-r3/rn)

        # coordinates to degrees
        x = degrees(x)
        y = degrees(y)

        col = int(COFF + round(x*aux16*CFAC))
        lin = int(LOFF + round(y*aux16*LFAC))

        xmin = [1, 1, 1, 40, 1]
        xmax = [1701, 2211, 1211, 701, 3712]
        ymin = [1, 1, 1, 1, 1]
        ymax = [651, 1151, 1191, 1511, 3712]

        if col <= xmax[iarea]+5 and col >= xmin[iarea]-5 and\
                lin <= ymax[iarea]+5 and lin >= ymin[iarea]-5:
            return col-1, lin-1
        else:
            return None, None 


def is_valid_file(parser, arg):
    """Description: Test if input conf file is valid.
                    This function is used with argparser.
    """
    return arg if Path(arg).is_file() else\
        parser.error("The file %s does not exist! " % arg)


def _log_level_string_to_int(log_level_string):
    """Description: Test log leve string given in argparse.
    """
    if not (log_level_string in _LOG_LEVEL_STRINGS):
        message = 'invalid choice: {0} (choose from'\
                      ' {1})'.format(log_level_string, _LOG_LEVEL_STRINGS)
        raise argparse.ArgumentTypeError(message)
    log_level_int = getattr(logging, log_level_string, logging.INFO)
    # check the logging log_level_choices have not changed
    # from our expected values
    assert isinstance(log_level_int, int)
    return log_level_int


def _prepare_geo():
    """Description: prepare argparse arguments to project or reproject file.
                    If proj_out and file_rep are defined - reproject dataset
                    else project dataset.
    """
    # get conf filename from argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-f", "--input_file", required=True,
        help="input filename (LSASAF HDF5)", metavar='FILE',
        type=lambda x: is_valid_file(parser, x)
    )
    parser.add_argument(
        "-p", "--product_name", required=True,
        help="dataset product name. please check HDF5 file.",
        type=str)
    parser.add_argument(
        "-lat", "--latitude", required=False,
        help="if you want clip dataset please parse latitude -lat 34 44"\
             "-lat lati latf",
        type=int, nargs="+")
    parser.add_argument(
        "-lon", "--longitude", required=False,
        help="if you want clip dataset please parse longitude -lon -10 -4"\
             "-lon loni lonf",
        type=int, nargs="+")
    parser.add_argument(
        "-p_in", "--proj_in", required=True,
        help="Proj in, in this case is always geos proj4 "\
             "+proj=geos +h=35785831 +a=6378169 +b=6356583.8 +no_defs",
        type=str)
    parser.add_argument(
        "-p_out", "--proj_out", required=False,
        help="proj out (EPSG) e.g 4326; this parameter is used in reproject",
        type=int)
    parser.add_argument(
        "-drv", "--drive_tp", required=True,
        help="Drive for output file (e.g. GTiff) to save to geotiff",
        type=str)
    parser.add_argument(
        "-f_geo", "--file_geo", required=False,
        help="output filename for geostationary projection", metavar="FILE")
    parser.add_argument(
        "-f_rep", "--file_rep", required=False,
        help="output filename for reproject method", metavar="FILE")
    parser.add_argument('-v', '--log_level', default='INFO', dest='log_level',
                        type=_log_level_string_to_int, nargs='?',
                        help='Set the logging output'
                             ' level. {0}'.format(_LOG_LEVEL_STRINGS))
    args = parser.parse_args()

    # formatter log; set configurations and get logger
    formatter = '[%(asctime)s - %(filename)s:%(lineno)s - '\
                '%(funcName)s() - %(levelname)s ] %(message)s'
    logging.basicConfig(format=formatter, level=args.log_level)

    # get dataset from hdf5
    logging.info("Input File: %s; Product Name: %s: " % (args.input_file, args.product_name))
    dset, llx, lly, urx, ury, mval = get_dataset(
        args.input_file, args.product_name, args.latitude, args.longitude)
    # Reproject Data if exist proj_out and file repro
    if args.proj_out and args.file_rep:
        logging.info("Reproject dataset: Output file - %s " % args.file_rep)
        logging.info("Reproject dataset: Output projection - %i " % args.proj_out)
        reproject_dset(dset, llx, lly, urx, ury, args.proj_in, args.proj_out,
                       args.drive_tp, args.file_rep)
    # Project
    else:
        logging.info("Project dataset: Output file - %s " % args.file_geo)
        logging.info("Project dataset: Projection - %s " % args.proj_in)
        project_dset(dset, llx, lly, urx, ury, args.proj_in, args.drive_tp,
                     args.file_geo)


if __name__ == "__main__":
    _prepare_geo()    
