README:
-------

This package allows you to assign projection and reproject the LSASAF geostationary-products (HDF5).


Installation:
-------------

### Requirements:

* Linux
* Python 3.3 and up

`$ python setup.py install`



Usage:
------

### Command Line

* Assign geostationary projection:
`$ geo_lsasaf -f /input_file/ -p "product_name" -p_in "+proj=geos +h=35785831 +a=6378169 +b=6356583.8 +no_defs" -drv "GTiff" -f_geo /output_file_geo/`

* Reproject (e.g. EPSG: 4326; GTiff - you can choose other available gdal drivers like netCDF4; lat=[33,44], lon=[-10,-4]):
`$ geo_lsasaf -f /input_file/ -p "product_name" -p_in "+proj=geos +h=35785831 +a=6378169 +b=6356583.8 +no_defs" -p_out 4326 -drv "GTiff" -f_rep /output_file_rep/ -lat 34 44 -lon -10 -4`

### e.g. ipython

`from geolsasaf import geo_lsasaf`

`dset, llx, lly, urx, ury, mval = geo_lsasaf.get_dataset(input_file, protuct_name)`

`geo_lsasat.project_dset(dset, llx, lly, urx, ury, mval, "+proj=geos +h=35785831 +a=6378169 +b=6356583.8 +no_defs", "GTiff", output_geo)`

`geo_lsasaf.reproject_dset(dset, llx, lly, urx, ury, mval, "+proj=geos +h=35785831 +a=6378169 +b=6356583.8 +no_defs", 4326, "GTiff", output_rep)`



Contributing:
-------------

Please open an issue first to discuss what do you like to change.
