#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Setup
"""

import os

try:
    from setuptools import setup
except ImportError:
    from disutils.core import setup


# Allow setup to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

# README
README = open(os.path.join(os.path.dirname(__file__), "README.rst")).read()

setup(
    name='geo_lsasaf',
    version='1.0',
    packages=['geolsasaf'],
    author='Nuno Simoes',
    author_email='nunosimoes58@gmail.com',
    description='project and reproject lsasaf products (MSG-Disk or user area)',
    install_requires=[
        'argparse==1.1',
        'numpy==1.15.1',
        'h5py==2.8.0',
    ],
    long_description=README,
    classifiers=[
        'Topic :: project and reproject LSASAF products (MSG-Disk or user area)',
        'Programming Language :: Python :: 3.5.6',
    ],
    entry_points={
        'console_scripts': [
            'geo_lsasaf = geolsasaf.geo_lsasaf:_prepare_geo',
        ],
    },
)
